module Eval (evaluateHaskellExpression) where

import qualified Language.Haskell.Interpreter as Hint

evaluateHaskellExpression :: String -> IO (Either Hint.InterpreterError String)
evaluateHaskellExpression expression = Hint.runInterpreter $ do
  Hint.setImports ["Prelude", "Data.List"] -- se pueden importar otros o que los pase el usuario
  Hint.interpret ("show (" ++ expression ++ ")") (Hint.as :: String)