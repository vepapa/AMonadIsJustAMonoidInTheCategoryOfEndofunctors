# AMonadIsJustAMonoidInTheCategoryOfEndofunctors
A Monad is just a Monoid in the Category of Endofunctors, **_what's the problem?_**

## Installation
Copy `Config.hs.tmp` to `Config.hs` and modify its values.
```
cabal update
cabal install aeson tls http-client http-client-tls hint uri-encode
ghc Main.hs
```

## Usage
```./Main```

## TODOs
- Working stack or cabal template and build
- More commands
- Others in project issues
