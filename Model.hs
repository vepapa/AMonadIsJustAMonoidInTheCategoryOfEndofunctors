{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Model where

import Data.Aeson
import Data.Aeson.TH
import GHC.Generics

data Message = Message
  { message_id :: Int,
    from :: User,
    chat :: Chat,
    date :: Int,
    text :: Maybe String
  }
  deriving (Show, Generic)

data User = User
  { id :: Int,
    is_bot :: Bool,
    first_name :: String,
    last_name :: Maybe String,
    username :: Maybe String,
    language_code :: Maybe String
  }
  deriving (Show, Generic)

data Chat = Chat
  { chat_id :: Int
  }
  deriving (Show, Generic)

data Update = Update
  { update_id :: Int,
    message :: Maybe Message
  }
  deriving (Show, Generic)

data TelegramResponse = TelegramResponse
  { ok :: Bool,
    result :: [Update]
  }
  deriving (Show, Generic)

$(deriveJSON defaultOptions ''Message)
$(deriveJSON defaultOptions ''User)
$(deriveJSON defaultOptions ''Update)
$(deriveJSON defaultOptions ''TelegramResponse)

-- Estos van separado porque comparte id
instance FromJSON Chat where
  parseJSON = withObject "Chat" $ \v -> do
    chat_id <- v .: "id"
    return Chat {chat_id = chat_id}

instance ToJSON Chat where
  toJSON = genericToJSON defaultOptions