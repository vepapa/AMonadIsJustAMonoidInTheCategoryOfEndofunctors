{-# LANGUAGE OverloadedStrings #-}

module Bot (getUpdates) where

import Config (telegramToken)
import Control.Monad (when)
import Data.Aeson
import Data.List (isPrefixOf)
import Data.Maybe (fromMaybe)
import Eval (evaluateHaskellExpression)
import Language.Haskell.Interpreter
import Model
import Network.HTTP.Client
import Network.HTTP.Client.TLS
import Network.URI.Encode as UriEncode (encode)

telegramBaseUrl :: String
telegramBaseUrl = "https://api.telegram.org/bot" ++ telegramToken

handleUpdates :: [Update] -> IO ()
handleUpdates [] = return ()
handleUpdates (update : rest) = do
  case message update of
    Just msg -> do
      when (isCommand "/peli" (text msg)) $ do
        -- TODO: Ver cómo modularizar esto para agregar nuevos comandos
        sendMessage (chat_id (chat msg)) "@VGV4dA @sasyaOperaO @abloobloo @userdemierda @om3tcw @untruehdblp002"
        handleUpdates rest -- continue
      evalResult <- evaluateHaskellExpression (fromMaybe "" (text msg))
      case evalResult of
        Right result -> sendMessage (chat_id (chat msg)) result
        Left result -> handleUpdates rest
      handleUpdates rest
    Nothing -> handleUpdates rest

isCommand :: String -> Maybe String -> Bool
isCommand cmd (Just text) = cmd `isPrefixOf` text
isCommand _ _ = False

sendMessage :: Int -> String -> IO ()
sendMessage chatId text = do
  let url = telegramBaseUrl ++ "/sendMessage"
      request = parseRequest_ $ url ++ "?chat_id=" ++ show chatId ++ "&text=" ++ UriEncode.encode text
  manager <- newManager tlsManagerSettings
  _ <- httpLbs request manager
  return ()

getUpdates :: Int -> IO ()
getUpdates offset = do
  let url = telegramBaseUrl ++ "/getUpdates"
      request = parseRequest_ $ url ++ "?offset=" ++ show offset
  manager <- newManager tlsManagerSettings
  response <- httpLbs request manager
  case decode (responseBody response) of
    Just (TelegramResponse True updates) ->
      if null updates
        then getUpdates offset
        else do
          let newOffset = maximum $ map update_id updates
          handleUpdates updates
          getUpdates (newOffset + 1)
    Nothing -> putStrLn "Error!"
